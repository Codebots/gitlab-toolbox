FROM registry.gitlab.com/gitlab-org/build/cng/gitlab-toolbox-ee:v15.6.0
USER root

RUN psql --version \
	pg_dump --version \
	&& curl https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add - \
	&& sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt bullseye-pgdg main" > /etc/apt/sources.list.d/pgdg.list' \
	&& apt-get update \
	&& apt-get install -y wget nano postgresql-client-15 \
	&& psql --version \
	&& pg_dump --version

USER git:git
